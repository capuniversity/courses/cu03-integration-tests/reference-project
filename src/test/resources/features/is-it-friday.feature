# Feature starsts with `Feature:` keyword, followed by feature name
Feature: Is it Friday yet?
  Everybody wants to know when it's Friday
# description, can be multiline

  # our feature has 1 scenario, scenario corresponds to `example` in BDD
  Scenario: Sunday isn't Friday
    # we have 3 steps
    Given today is Sunday
    When I ask whether it's Friday yet
    Then I should be told "Nope"

  # usually you have multiple scenarios in 1 feature
  Scenario: Friday is Friday
    Given today is Friday
    When I ask whether it's Friday yet
    Then I should be told "TGIF"