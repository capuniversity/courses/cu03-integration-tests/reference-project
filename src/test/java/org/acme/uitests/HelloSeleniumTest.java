package org.acme.uitests;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class HelloSeleniumTest {
    // Standard JUnit test
    @Test
    public void test() {
        // we create a new instance of Chrome driver(new chrome browser controlled by webdriver)
        WebDriver driver = new ChromeDriver();
        // a simple declarative wait stmt
        WebDriverWait wait = new WebDriverWait(driver, 10);
        try {
            // open google URL
            driver.get("https://google.com/ncr");
            // find element that has `name` attribute set to `q`
            // send keys = enter text simulating user and keyboard
            driver.findElement(By.name("q")).sendKeys("Selenium" + Keys.ENTER);
            // we pressed enter, so google will show us the results
            // but it can take some time(although for google probably like 2 ms :) )
            // therefore we set up wait, we block for a total maximum 10 seconds,
            // each 500ms(default, we can customize by passing 3rd param) we execute our check
            // which in this case is: try to find element using the css selector
            // if not found, wait 500ms and try again
            // if successful, we get back the element we waited for
            // if not we get exception
            WebElement firstResult = wait.until(presenceOfElementLocated(By.cssSelector("#rso > div:nth-child(1) > div > div.r > a > h3")));
            // we got our element, print its content
            System.out.println(firstResult.getAttribute("textContent"));
            // we can also print other attributes - just see javadoc
            System.out.println("Result is: "+firstResult.getTagName() + " with text "+ firstResult.getText() + " and these classes "+ firstResult.getAttribute("class"));

        } finally {
            driver.quit();
        }
    }
}
  