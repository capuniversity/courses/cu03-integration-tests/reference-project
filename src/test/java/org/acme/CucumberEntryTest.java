package org.acme;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {
                "json:target/cucumber-report/cucumber.json",
                "io.qameta.allure.cucumber5jvm.AllureCucumber5Jvm"
        },
        dryRun = false, strict = true,
        features = "src/test/resources/features",
        glue = {"org.acme.steps"})
public class CucumberEntryTest {
}
