package org.acme.steps;

import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

// this is our business logic that we want to test
class IsItFriday {
    static String isItFriday(String today) {
        return "Friday".equalsIgnoreCase(today) ? "TGIF" : "Nope";
    }
}

// Step definitions can be in any java file
public class StepDefinitions {
    private String today;
    private String actualAnswer;
    private ChromeDriver driver;

    // Cucumber hook after scenario finishes
    @After()
    public void closeBrowser(Scenario scenario) {
        if (driver != null) {
            byte[] screenshot = driver.getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png", "Screenshot_" + scenario.getName() + "_" + scenario.getLine());
            driver.quit();
            driver = null;
        }
    }

    // Step def is marked by annotating a public method with one of Given,When,Then annotations
    @Given("today is Sunday")
    public void today_is_Sunday() {
        // simple way to store state, we would normally use some CDI here
        today = "Sunday";
    }

    @Given("today is Friday")
    public void today_is_Friday() {
        today = "Friday";
    }

    @When("I ask whether it's Friday yet")
    public void i_ask_whether_it_s_Friday_yet() {
        actualAnswer = IsItFriday.isItFriday(today);
    }

    @Then("I should be told {string}")
    public void i_should_be_told(String expectedAnswer) {
        // simple verification using Junit asserts, we could use any assert library here
        assertEquals(expectedAnswer, actualAnswer);
    }

    @Given("I am on the Google search page")
    public void i_am_on_the_Google_search_page() {
        driver = new ChromeDriver();
        // open google URL
        driver.get("https://google.com/ncr");
    }


    @When("I search for {string}")
    public void i_search_for(String string) {
        // find element that has `name` attribute set to `q`
        // send keys = enter text simulating user and keyboard
        driver.findElement(By.name("q")).sendKeys(string + Keys.ENTER);
    }


    @Then("first result should start with {string}")
    public void first_result_should_start_with(String string) {
        WebDriverWait wait = new WebDriverWait(driver, 10);

        WebElement firstResult = wait.until(presenceOfElementLocated(By.cssSelector("#rso > div:nth-child(1) > div > div.r > a > h3")));
        // we got our element, print its content
        String elementText = firstResult.getAttribute("textContent");
        assertTrue(elementText.startsWith(string));
    }


}
